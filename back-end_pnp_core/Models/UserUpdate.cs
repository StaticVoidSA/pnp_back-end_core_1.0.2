﻿using System;

namespace back_end_pnp_core.Models
{
    public class UserUpdate
    {
        public string firstName { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string _email { get; set; }
        public string password { get; set; }
        public DateTime doj { get; set; }
        public string userRole { get; set; }
    }
}
