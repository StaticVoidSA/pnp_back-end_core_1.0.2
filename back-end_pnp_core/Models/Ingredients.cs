﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public class Ingredients
    {
        public int recID { get; set; }
        public string brand { get; set; }
        public string category { get; set; }
        public string quantity { get; set; }
        public string title { get; set; }
        public decimal price { get; set; }
        public string barcode { get; set; }
        public string ingredientImage { get; set; }
    }
}
