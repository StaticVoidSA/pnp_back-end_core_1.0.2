﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public class SignupResponse
    {
        public string firstName { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string encPassword { get; set; }
        public bool success { get; set; } = false;
    }
}
