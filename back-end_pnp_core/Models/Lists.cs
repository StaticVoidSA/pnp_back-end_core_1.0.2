﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public class Lists
    {
        public int userID { get; set; }
        public string listName { get; set; }
        public int itemID { get; set; }
    }
}
