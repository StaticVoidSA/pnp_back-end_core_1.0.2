﻿namespace back_end_pnp_core.Models
{
    public class ResetResponse
    {
        public string email { get; set; }
        public bool emailSent { get; set; }
    }
}
