﻿namespace back_end_pnp_core.Models
{
    public class CompleteResponse
    {
        public string email { get; set; }
        public bool passwordChanged { get; set; }
    }
}
