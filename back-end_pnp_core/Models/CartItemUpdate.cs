﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public class CartItemUpdate
    {
        public int userID { get; set; }
        public int cartItemID { get; set; }
        public int quantity { get; set; }
    }
}
