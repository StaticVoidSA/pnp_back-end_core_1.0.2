﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public class Catalogue
    {
        public string category { get; set; }
        public string brand { get; set; }
    }
}
