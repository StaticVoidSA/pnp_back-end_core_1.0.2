﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class CompleteResetDTO
    {
        public string email { get; set; }
        public string password { get; set; }
        public bool isComplete { get; set; }
        public string hash { get; set; }
    }
}
