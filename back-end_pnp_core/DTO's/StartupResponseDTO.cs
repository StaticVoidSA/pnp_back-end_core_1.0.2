﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class StartupResponseDTO
    {
        public int userID { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public DateTime doj { get; set; }
        public string userRole { get; set; }
    }
}
