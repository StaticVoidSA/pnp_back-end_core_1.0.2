﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class CatalogueDTO
    {
        public string category { get; set; }
        public string brand { get; set; }
    }
}
