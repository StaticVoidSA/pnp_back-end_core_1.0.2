﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class CompleteResponseDTO
    {
        public string email { get; set; }
        public bool passwordChanged { get; set; }
    }
}
