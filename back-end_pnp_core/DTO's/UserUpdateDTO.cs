﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class UserUpdateDTO
    {
        public string firstName { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string _email { get; set; }
        public string password { get; set; }
        public DateTime doj { get; set; }
        public string userRole { get; set; }
    }
}
