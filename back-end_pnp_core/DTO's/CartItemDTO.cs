﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class CartItemDTO
    {
        public int userID { get; set; }
        public int cartItemID { get; set; }
        public string title { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public string barcode { get; set; }
        public string brand { get; set; }
    }
}
