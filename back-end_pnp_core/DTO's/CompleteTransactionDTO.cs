﻿using System;
using back_end_pnp_core.Models;

namespace back_end_pnp_core.DTOs
{
    public enum SelectedChoice
    {
        Collection,
        Delivery
    }

    public class CompleteTransactionDTO
    {
        public string userName { get; set; }
        public string userEmail { get; set; }
        public int userID { get; set; }
        public PaidItems[] paidItems { get; set; }
        public string selectedShop { get; set; }
        public string selectedAddress { get; set; }
        public DateTime? selectedCollectionDate { get; set; }
        public DateTime transactionDate { get; set; }
        public DateTime? deliveryDate { get; set; }
        public SelectedChoice userSelection { get; set; }
    }
}
