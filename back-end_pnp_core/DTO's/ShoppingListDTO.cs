﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class ShoppingListDTO
    {
        public int userID { get; set; }
        public string shoppingListName { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public int shoppingListID { get; set; }
        public int itemID { get; set; }
    }
}
