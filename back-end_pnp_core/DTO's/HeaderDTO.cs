﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class HeaderDTO
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
