﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class RecipeDTO
    {
        public int recipeID { get; set; }
        public string title { get; set; }
        public string recipeImage { get; set; }
    }
}
