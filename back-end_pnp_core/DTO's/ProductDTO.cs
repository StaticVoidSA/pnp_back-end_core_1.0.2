﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.DTO_s
{
    public class ProductDTO
    {
        public int productID { get; set; }
        public string title { get; set; }
        public string category { get; set; }
        public string brand { get; set; }
        public string uri { get; set; }
        public double price { get; set; }
        public string description { get; set; }
        public string features { get; set; }
        public string usage { get; set; }
        public string quantity { get; set; }
        public string barcode { get; set; }
    }
}
