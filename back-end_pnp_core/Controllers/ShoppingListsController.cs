﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingListsController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Shopping Lists Controller Called...");
        ShoppingListService service;

        public ShoppingListsController(IConfiguration config)
        {
            this._config = config;
            service = new ShoppingListService(_config);
        }

        [HttpPost, Route("/api/shoppingLists/createShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> createShoppingList(ShoppingListDTO list)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/createShoppingList");

            if (list.shoppingListName == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.CreateList(list);
                logger.LogWrite("Create Shopping List Complete...");
                return Ok(result);
            }
        }

        [HttpGet, Route("/api/shoppingLists/getUserShoppingList")]
        [ProducesDefaultResponseType(typeof(List<ListsDTO>))]
        public async Task<IActionResult> getUserShoppingList(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/getUserShoppingList");
            List<ListsDTO> response = await service.GetUserLists(userID);
            logger.LogWrite("Get User Shopping List Complete...");
            return Ok(response);
        }

        [HttpDelete, Route("/api/shoppingLists/deleteShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteShoppingList(int itemID, int userID)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/deleteShoppingList");
            bool result = await service.DeleteShoppingList(itemID, userID);
            logger.LogWrite("Delete Shopping List Complete...");
            return Ok(result);
        }

        [HttpPost, Route("/api/shoppingLists/addToShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToShoppingList(ShoppingListDTO list)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/addToShoppingList");
            bool result = await service.AddToShoppingList(list);
            logger.LogWrite("Add To Shopping List Complete...");
            return Ok(result);
        }

        [HttpGet, Route("/api/shoppingLists/getListItems")]
        [ProducesDefaultResponseType(typeof(List<ShoppingListDTO>))]
        public async Task<IActionResult> getListItems(int userID, string listName)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/getListItems");
            List<ShoppingListDTO> response = await service.GetListItems(userID, listName);
            logger.LogWrite("Get Shopping List Items Complete...");
            return Ok(response);
        }

        [HttpDelete, Route("/api/shoppingLists/deleteFromList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteFromList(int listID, int ID)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/deleteFromList");
            bool result = await service.DeleteFromShoppingList(listID, ID);
            logger.LogWrite("Delete From List Complete...");
            return Ok(result);
        }

        [HttpGet, Route("/api/shoppingLists/getUserDetails")]
        [ProducesDefaultResponseType(typeof(UserDTO))]
        public async Task<IActionResult> getuserDetails(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/getuserDetails");

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                UserDTO response = await service.GetUserDetails(userID);
                logger.LogWrite("Get User Details Complete...");
                return Ok(response);
            }
        }

        [HttpGet, Route("/api/shoppingLists/getListCount")]
        [ProducesDefaultResponseType(typeof(int))]
        public async Task<IActionResult> getListCount(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/shoppingLists/getListCount");

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                int response = await service.GetListsCount(userID);
                logger.LogWrite("Get User List Count Complete...");
                return Ok(response);
            }
        }
    }
}
