﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Search Controller Called...");
        SearchService service;

        public SearchController(IConfiguration config)
        {
            this._config = config;
            service = new SearchService(_config);
        }

        [AllowAnonymous]
        [HttpGet, Route("api/search/catalogue")]
        [ProducesDefaultResponseType(typeof(IEnumerable<SearchResponseDTO>))]
        public async Task<IActionResult> catalogue()
        {
            logger.LogWrite("https://localhost:44398/api/search/catalogue");
            IEnumerable<SearchResponseDTO> response = await service.catalogue();
            logger.LogWrite("Search Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("api/search/searchAll")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchAll(SearchRequestDTO request)
        {
            logger.LogWrite("https://localhost:44398/api/search/searchAll");
            List<SearchResponseDTO> response = await service.searchAll(request);
            logger.LogWrite("Search Complete...");
            return Ok(response);
        }
    }
}