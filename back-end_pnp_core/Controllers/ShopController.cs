﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Shop Controller Called...");
        ShopService service;

        public ShopController(IConfiguration config)
        {
            this._config = config;
            service = new ShopService(_config);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/shop/startup")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> get() 
        {
            logger.LogWrite("https://localhost:44398/api/shop/startup");
            List<SearchResponseDTO> response = await service.onStartup();
            logger.LogWrite("Startup Search Complete...");
            return Ok(response);

        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchBrand")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchBrand(SearchRequestDTO request)
        {
            logger.LogWrite("https://localhost:44398/api/shop/searchBrand");

            if (request.brand == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByBrand(request);
                logger.LogWrite("Search Brand Complete...");
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchPrice")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchPrice(SearchRequestDTO request)
        {
            logger.LogWrite("https://localhost:44398/api/shop/searchPrice");

            if (double.IsNaN(request.price))
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByPrice(request);
                logger.LogWrite("Search Price Complete...");
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchQuantity")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchQuantity(SearchRequestDTO request)
        {
            logger.LogWrite("https://localhost:44398/api/shop/searchQuantity");

            if (request.quantity == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByQuantity(request);
                logger.LogWrite("Search Quantity Complete...");
                return Ok(response);
            }
        }
    }
}