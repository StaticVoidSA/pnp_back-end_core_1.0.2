﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipesController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Recipes Controller Called...");
        RecipesService service;

        public RecipesController(IConfiguration config)
        {
            this._config = config;
            service = new RecipesService(_config);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/startupRecipes")]
        [ProducesDefaultResponseType(typeof(List<RecipeDTO>))]
        public async Task<IActionResult> startupRecipes()
        {
            logger.LogWrite("https://localhost:44398/api/recipes/startupRecipes");
            List<RecipeDTO> response = await service.startupRecipes();
            logger.LogWrite("Startup Recipes Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/getRecipe")]
        [ProducesDefaultResponseType(typeof(List<RecipeCompleteDTO>))]
        public async Task<IActionResult> getRecipe(int recID)
        {
            logger.LogWrite("https://localhost:44398/api/recipes/getRecipe");
            List<RecipeCompleteDTO> response = await service.getRecipe(recID);
            logger.LogWrite("Get Recipe Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/getRecipes")]
        [ProducesDefaultResponseType(typeof(List<RecipeCompleteDTO>))]
        public async Task<IActionResult> getRecipes()
        {
            logger.LogWrite("https://localhost:44398/api/recipes/getRecipe");
            List<RecipeCompleteDTO> response = await service.getAllRecipes();
            logger.LogWrite("Get Recipes Complete...");
            return Ok(response);
        }
    }
}
