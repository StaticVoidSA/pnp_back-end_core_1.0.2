﻿using back_end_pnp_core.DAOs;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Cart Controller Called...");
        CartService service;
        PaidItemsService paidService;

        public CartController(IConfiguration config)
        {
            this._config = config;
            service = new CartService(_config);
            paidService = new PaidItemsService(_config);
        }

        [HttpGet, Route("/api/cart/getItems")]
        [ProducesDefaultResponseType(typeof(List<CartItemDTO>))]
        public async Task<IActionResult> getItems(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/cart/getItems");

            if (userID < 0 )
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<CartItemDTO> results = await service.getCartItems(userID);

                logger.LogWrite("Get Items Complete...");
                return Ok(results);
            }
        }

        [HttpPost, Route("/api/cart/addToCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToCart(CartDTO item)
        {
            logger.LogWrite("https://locahost:44398/api/cart/addToCart");

            bool result = await service.AddToCart(item);

            logger.LogWrite("Add To Cart Complete...");
            return Ok(result);
        }


        [HttpDelete, Route("/api/cart/removeFromCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeFromCart(int cartItemID, int cartID)
        {
            logger.LogWrite("https://localhost:44398/api/cart/removerFromCart");

            if (cartItemID < 0 || cartID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.RemoveFromCart(cartItemID, cartID);
                logger.LogWrite("Remove From Cart Complete...");
                return Ok(result);
            }
        }

        [HttpGet, Route("/api/cart/cartCount")]
        [ProducesDefaultResponseType(typeof(int))]
        public async Task<IActionResult> cartCount(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/cart/cartCount");
            int result = await service.cartCount(userID);

            logger.LogWrite("Cart Count Complete...");
            return Ok(result);
        }

        [HttpPut, Route("/api/cart/updateCartItem")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateCartItem(CartItemUpdateDTO item)
        {
            logger.LogWrite("https://locahost:44398/api/cart/updateCartItem");

            if (item.userID < 0 || item.cartItemID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.updateCartItem(item);

                logger.LogWrite("Update Cart Item Complete...");
                return Ok(result);
            }
        }

        [HttpDelete, Route("/api/cart/clearCartItems")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeItems(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/cart/removeItems");

            if (userID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.removeAllItems(userID);

                if (!result)
                {
                    return BadRequest("An Error Occurred");
                }

                logger.LogWrite("Removed All Items For User: " + userID);
                return Ok(result);
            }
        }

        [HttpPost, Route("/api/cart/addToPaidItems")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToPaidItems(CartDTO item)
        {
            logger.LogWrite("https://localhost:44398/api/cart/addToPaidItems");

            bool result = await paidService.AddToPaidFor(item);

            if (!result)
            {
                return BadRequest("An Error Occurred");
            }

            logger.LogWrite("Added " + item.title + " To Paid Items For User: " + item.userID);
            return Ok(result);
        }
    }
}
