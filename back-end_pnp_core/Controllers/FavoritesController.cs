﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoritesController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Favorites Controller Called");
        FavoritesService service;

        public FavoritesController(IConfiguration config)
        {
            this._config = config;
            service = new FavoritesService(_config);
        }

        [HttpGet, Route("/api/favorites/getfavorites")]
        [ProducesDefaultResponseType(typeof(List<FavoritesDTO>))]
        public async Task<IActionResult> getFavorites(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/favorites/getFavorites");

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else 
            {
                List<FavoritesDTO> response = await service.getFavorites(userID);
                logger.LogWrite("Get Favorites Complete...");
                return Ok(response);
            }
        }

        [HttpPost, Route("/api/favorites/addToFavorites")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToFavorites(FavoritesDTO item)
        {
            logger.LogWrite("https://localhost:44398/api/favorites/addToFavorites");

            if (item.userID < 0 || item.ID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.addToFavorites(item);
                logger.LogWrite("Add To Favorites Complete...");
                return Ok(result);
            }
        }

        [HttpDelete, Route("/api/favorites/removeFromFavorites")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeFromFavorites(int ID, int favID)
        {
            logger.LogWrite("https://localhost:44398/api/favorites/removeFromFavorites");

            if (ID < 0 || favID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.removeFromFavorites(ID, favID);
                logger.LogWrite("Remove From Favorites Complete...");
                return Ok(result);
            }
        }

        [HttpPost, Route("/api/favorites/addToCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToCart(CartDTO item)
        {
            logger.LogWrite("https://localhost:44398/api/favorites/removeFromFavorites");

            if (item.userID < 0 || item.cartID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.addToCart(item);
                logger.LogWrite("Add To Cart From Favorites Complete...");
                return Ok(response);
            }
        }
    }
}
