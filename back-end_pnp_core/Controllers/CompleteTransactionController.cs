﻿using System.Threading.Tasks;
using back_end_pnp_core.DAOs;
using back_end_pnp_core.DTOs;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompleteTransactionController : Controller
    {
        private readonly IConfiguration _config;
        CompleteTransactionService service;
        SendEmailService emailService;
        LoggerService logger = new LoggerService("Complete Transaction Controller Called...");

        public CompleteTransactionController(IConfiguration config)
        {
            this._config = config;
            service = new CompleteTransactionService(_config);
            emailService = new SendEmailService(_config);
        }

        [HttpPost, Route("/api/completeTransaction/complete")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> CompleteTransaction(CompleteTransactionDTO transaction)
        {
            bool response = false;

            if (transaction.userID < 0 || transaction.userEmail == null)
            {
                return BadRequest("Bad Request");
            }

            logger.LogWrite("https://localhost:44398/api/completeTransaction/complete");

            switch (transaction.userSelection.ToString())
            {
                case "Collection":
                    response = await service.CompleteCollection(transaction);
                    break;
                case "Delivery":
                    response = await service.CompleteDelivery(transaction);
                    break;
                default:
                    return BadRequest("Bad Request");
            }

            bool emailResponse = await emailService.sendReceiptEmail(transaction.userEmail, transaction);

            if (emailResponse)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest("Unable to send email to user");
            }
        }
    }
}
