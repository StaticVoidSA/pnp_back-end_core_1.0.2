﻿using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("https://localhost:44398/api/auth " +
            "\nAuthentication Controller Called");
        private readonly IJWTAuthenticationManager jWTAuthenticationManager;
        AuthService service;

        public AuthController(IConfiguration config, IJWTAuthenticationManager jWTAuthenticationManager)
        {
            this._config = config;
            service = new AuthService(_config);
            this.jWTAuthenticationManager = jWTAuthenticationManager;
        }

        [AllowAnonymous]
        [HttpPost, Route("api/auth/signup")]
        [ProducesDefaultResponseType(typeof(SignupResponseDTO))]
        public async Task<IActionResult> signup(UserDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/auth/signup");
            SignupResponseDTO response = await service.signup(user);
            logger.LogWrite("Sign Up Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("api/auth/login")]
        [ProducesDefaultResponseType(typeof(LoginResponseDTO))]
        public async Task<IActionResult> login(UserDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/auth/login");

            if (user.password == null || user.email == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                var token = jWTAuthenticationManager.Authenticate(user.email, user.password);
                LoginResponseDTO response = await service.login(user, token);
                logger.LogWrite("Login Complete...");
                return Ok(response);
            }
        }
    }
}
