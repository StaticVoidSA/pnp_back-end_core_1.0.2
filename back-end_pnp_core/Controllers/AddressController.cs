﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Address Controller Called...");
        AddressService service;

        public AddressController(IConfiguration config)
        {
            this._config = config;
            service = new AddressService(_config);
        }

        [HttpPost, Route("/api/address/createAddress")]
        [ProducesDefaultResponseType(typeof(string))]
        public async Task<IActionResult> createAddress(AddressDTO address)
        {
            if (address.addressID <= 0
                || address.userAddress == string.Empty
                || address.addressNickName == string.Empty)
            { 
                return BadRequest("Invalid Request");
            }
            else
            {
                string response = await service.createAddress(address);
                logger.LogWrite("Address Created Successfully For User:", address.addressID.ToString());
                return Ok(response);
            }
        }

        [HttpGet, Route("/api/address/getAddresses")]
        [ProducesDefaultResponseType(typeof(List<AddressDTO>))]
        public async Task<IActionResult> getAddresses(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<AddressDTO> response = await service.getAddresses(userID);
                logger.LogWrite("Addresses Returned To User:", userID.ToString());
                return Ok(response);
            }
        }

        [HttpDelete, Route("/api/address/deleteAddress")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteAddress(int addressID, int userID)
        {
            if (addressID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.deleteAddress(addressID, userID);
                logger.LogWrite("Deleted Address:", (addressID + " Successful For User: " + userID));
                return Ok(response);
            }
        }

        [HttpPut, Route("/api/address/updateAddress")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateAddress(AddressDTO address)
        {
            if (address.addressID <= 0
                || address.ID <= 0
                || address.userAddress == string.Empty
                || address.addressNickName == string.Empty)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.updateAddress(address);
                logger.LogWrite("Updated Address:", (address.addressNickName + " Successful For User: " + address.addressID));
                return Ok(response);
            }
        }
    }
}
