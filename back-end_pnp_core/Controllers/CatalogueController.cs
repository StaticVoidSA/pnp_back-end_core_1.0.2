﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogueController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Catalogue Controller Called...");
        CatalogueService service;

        public CatalogueController(IConfiguration config)
        {
            this._config = config;
            service = new CatalogueService(_config);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/catalogue/getCatalogueItems")]
        [ProducesDefaultResponseType(typeof(List<CatalogueDTO>))]
        public async Task<IActionResult> getCatalogueItems(string category)
        {
            logger.LogWrite("https://localhost:44398/api/catalogue/getCatalogueItems");

            if (category != null)
            {
                List<CatalogueDTO> response = await service.getCatalogues(category);
                logger.LogWrite("Get Catalogue Items Complete...");
                return Ok(response);
            }
            else
            {
                return BadRequest("Invalid Request");
            }
        }
    }
}