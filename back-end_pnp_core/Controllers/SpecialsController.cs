﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecialsController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Specials Controller Called...");
        SpecialsService service;

        public SpecialsController(IConfiguration config)
        {
            this._config = config;
            service = new SpecialsService(_config);
        }

        [HttpGet, Route("/api/specials/getSpecials")]
        [ProducesDefaultResponseType(typeof(List<SpecialsDTO>))]
        public async Task<IActionResult> getSpecials(string category) 
        {
            logger.LogWrite("https://localhost:44398/api/specials/getSpecials");

            if (category == null)
            {
                return BadRequest("Invlaid Request");
            }
            else
            {
                List<SpecialsDTO> response = await service.getSpecials(category);
                logger.LogWrite("Get Specials Complete...");
                return Ok(response);
            }
        }
    }
}
