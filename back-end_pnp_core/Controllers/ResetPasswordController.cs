﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class ResetPasswordController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Reset Password Controller Called...");
        ResetService service;

        public ResetPasswordController(IConfiguration config)
        {
            this._config = config;
            service = new ResetService(_config);
        }

        [AllowAnonymous]
        [HttpPost, Route("api/resetpassword/reset")]
        [ProducesDefaultResponseType(typeof(ResetResponseDTO))]
        public async Task<IActionResult> reset(UserDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/resetpassword/reset");

            if (user.userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                ResetResponseDTO response = await service.resetPassword(user);
                return Ok(response);
            }
        }


        [HttpPost, Route("api/resetpassword/complete")]
        [ProducesDefaultResponseType(typeof(CompleteResponseDTO))]
        public async Task<IActionResult> complete(CompleteResetDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/resetpassword/complete");

            if (user.email == null || user.password == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                CompleteResponseDTO response = await service.complete(user);
                logger.LogWrite("Reset Password Complete For User: ", user.email);
                return Ok(response);
            }
        }
    }
}
