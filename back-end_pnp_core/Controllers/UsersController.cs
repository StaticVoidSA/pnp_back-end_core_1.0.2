﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Users Controller Called...");
        UsersService service;

        public UsersController(IConfiguration config)
        {
            this._config = config;
            service = new UsersService(_config);
        }

        [HttpGet, Route("api/admin/getUsers")]
        [ProducesDefaultResponseType(typeof(List<UserDTO>))]
        public async Task<IActionResult> getUsers()
        {
            logger.LogWrite("https://localhost:44398/api/admin/getUsers");         
            List<UserDTO> response = await service.getUsers();
            logger.LogWrite("Get Users Details Complete...");
            return Ok(response);
        }

        [HttpPost, Route("api/admin/getUser")]
        [ProducesDefaultResponseType(typeof(UserDTO))]
        public async Task<IActionResult> getUser(UserDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/admin/getUser");
            UserDTO response = await service.getUser(user);
            logger.LogWrite("Get User Details Complete...");
            return Ok(response);
        }

        [HttpPost, Route("api/admin/deleteUser")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteUser(UserDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/admin/deleteUser");
            bool response = await service.deleteUser(user);
            logger.LogWrite("Delete User Complete...");
            return Ok(response);
        }

        [HttpPut, Route("api/admin/updateUser")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateUser(UserUpdateDTO user)
        {
            logger.LogWrite("https://localhost:44398/api/admin/updateUser");
            bool response = await service.updateUser(user);
            logger.LogWrite("Update User Complete...");
            return Ok(response);
        }
    }
}