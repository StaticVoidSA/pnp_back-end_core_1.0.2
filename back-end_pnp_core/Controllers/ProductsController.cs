﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Products Controller Called...");
        ProductsService service;

        public ProductsController(IConfiguration config)
        {
            this._config = config;
            service = new ProductsService(_config);
        }

        [AllowAnonymous]
        [HttpGet, Route("api/admin/getProducts")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> getProducts()
        {
            logger.LogWrite("https://localhost:44398/api/admin/getProducts");
            List<SearchResponseDTO> response = await service.getProducts();
            logger.LogWrite("Get Products Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("api/admin/getProduct")]
        [ProducesDefaultResponseType(typeof(ProductDTO))]
        public async Task<IActionResult> getProduct(ProductDTO product)
        {
            logger.LogWrite("https://localhost:44398/api/admin/getProduct");
            ProductDTO response = await service.getProduct(product);
            logger.LogWrite("Get Product Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPut, Route("api/admin/updateProduct")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateProduct(ProductUpdateDTO product)
        {
            logger.LogWrite("https://localhost:44398/api/admin/getProduct");
            bool response = await service.editProduct(product);
            logger.LogWrite("Update Product Complete...");
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("api/admin/deleteProduct")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteProduct(ProductDTO product)
        {
            logger.LogWrite("https://localhost:44398/api/admin/deleteProduct");
            bool response = await service.deleteProduct(product);
            logger.LogWrite("Delete Product Complete...");
            return Ok(response);
        }
    }
}
