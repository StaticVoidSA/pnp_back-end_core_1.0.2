﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end_pnp_core.DAOs;
using back_end_pnp_core.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveriesController : Controller
    {
        private readonly IConfiguration _config;
        DeliveriesService service;

        public DeliveriesController(IConfiguration config)
        {
            this._config = config;
            service = new DeliveriesService(_config);
        }

        [HttpGet, Route("/api/deliveries/getDeliveries")]
        [ProducesDefaultResponseType(typeof(IActionResult))]
        public async Task<IActionResult> getDeliveries(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("Bad Request");
            }

            List<DeliveryItemDTO> response = await service.getDeliveries(userID);

            return Ok(response);
        }
    }
}
