﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DAOs;
using back_end_pnp_core.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : Controller
    {
        private readonly IConfiguration _config;
        CollectionService service;

        public CollectionsController(IConfiguration config)
        {
            this._config = config;
            service = new CollectionService(_config);
        }

        [HttpGet, Route("/api/collections/getCollections")]
        [ProducesDefaultResponseType(typeof(IActionResult))]
        public async Task<IActionResult> getCollections(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("BadRequest");
            }

            List<CollectionItemDTO> response = await service.getCollections(userID);

            return Ok(response);
        }
    }
}
