﻿using System.Collections.Generic;
using System.Threading.Tasks;
using back_end_pnp_core.DAOs;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end_pnp_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaidItemsController : Controller
    {
        private readonly IConfiguration _config;
        private PaidItemsService service;
        LoggerService logger = new LoggerService("Paid For Controller Called.");

        public PaidItemsController(IConfiguration config)
        {
            this._config = config;
            this.service = new PaidItemsService(_config);
        }


        [HttpGet, Route("/api/paidItems/getItems")]
        [ProducesDefaultResponseType(typeof(List<CartDTO>))]
        public async Task<IActionResult> getItems(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/paidItems/getItems?userID=" + userID);

            if (userID <= 0)
            {
                logger.LogWrite("Bad Request From User: " + userID);
                return BadRequest("Invalid request sent");
            }
            else
            {
                List<CartDTO> results = await service.GetPaidFor(userID);
                logger.LogWrite("Returned All Paid Items For User: " + userID);
                return Ok(results);
            }
        }

        [HttpDelete, Route("/api/paidItems/clearAll")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> clearAll(int userID)
        {
            logger.LogWrite("https://localhost:44398/api/paidItems/clearAll?userID=" + userID);

            if (userID <= 0)
            {
                logger.LogWrite("Bad Request From User: " + userID);
                return BadRequest("Invalid request sent");
            }
            else
            {
                bool response = await service.ClearPaidItems(userID);
                logger.LogWrite("Cleared All Paid Items For User: " + userID);
                return Ok(response);
            }
        }
    }
}
