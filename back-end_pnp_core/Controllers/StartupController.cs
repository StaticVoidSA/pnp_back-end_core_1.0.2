﻿using System.Collections.Generic;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Models;
using back_end_pnp_core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace back_end_pnp_core.Controllers
{
    [ApiController]
    public class StartupController : ControllerBase
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Startup Controller Called");
        AuthService service;

        public StartupController(IConfiguration config)
        {
            this._config = config;
            service = new AuthService(_config);
        }

        [HttpGet, Route("startup")]
        [ProducesDefaultResponseType(typeof(List<StartupResponseDTO>))]
        public async IAsyncEnumerable<StartupResponseDTO> Get()
        {
            logger.LogWrite("https://localhost:44398/api/startup");
            List<StartupResponseDTO> response = await service.getUsers();
            logger.LogWrite("Startup Complete...");

            foreach (var user in response)
            {
                yield return user;
            }
        }
    }
}
