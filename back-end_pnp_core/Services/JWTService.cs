﻿using back_end_pnp_core.DTO_s;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace back_end_pnp_core.Services
{
    public interface IJWT
    {
        public string GenerateJSONWebToken(UserDTO userInfo);
        public Task<bool> CompareJWT(string input);
        public List<HeaderDTO> ExtractHeaderValues(IHeaderDictionary dictionary);
        public string FormatString(string input);
    }

    public class JWTService : IJWT
    {
        private IConfiguration _config;

        public JWTService(IConfiguration config)
        {
            this._config = config;
        }

        public string GenerateJSONWebToken(UserDTO userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.firstName),
                new Claim(JwtRegisteredClaimNames.Email, userInfo.email),
                new Claim("EncPassword", userInfo.password),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Audience"],
              claims,
              expires: DateTime.Now.AddHours(3),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<bool> CompareJWT(string input)
        {
            bool result;
            LocalStorageService service = new LocalStorageService();
            string _input = service.read();
            LoggerService logger = new LoggerService("Input JWT: " + input + "\nUser JWT: " + _input);

            if (input == _input)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public List<HeaderDTO> ExtractHeaderValues(IHeaderDictionary dictionary)
        {
            List<HeaderDTO> response = new List<HeaderDTO>();

            foreach (StringValues keys in dictionary.Keys)
            {
                HeaderDTO header = new HeaderDTO();
                header.Key = keys;
                header.Value = dictionary[keys];
                response.Add(header);
            }

            return response;
        }

        public string FormatString(string input)
        {
            string output = input.Replace("Bearer ", "");

            return output;
        }
    }
}
