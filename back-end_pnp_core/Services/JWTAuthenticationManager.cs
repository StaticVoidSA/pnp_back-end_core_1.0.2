﻿using back_end_pnp_core.Models;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace back_end_pnp_core.Services
{
    public interface IJWTAuthenticationManager
    {
        string Authenticate(string username, string password);
        public bool UserLoggedIn(string email, string password);
    }

    public class JWTAuthenticationManager : IJWTAuthenticationManager
    {
        private readonly string tokenKey;
        private readonly IConfiguration _config;

        public JWTAuthenticationManager(string tokenKey, IConfiguration config)
        {
            this.tokenKey = tokenKey;
            this._config = config;
        }

        public string Authenticate(string email, string password)
        {
            bool result = UserLoggedIn(email, password);

            if (!result)
            {
                return null;
            }
            else
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(tokenKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, email)
                    }),
                    Expires = DateTime.UtcNow.AddHours(1),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
        }

        public bool UserLoggedIn(string email, string password)
        {
            var encPassword = UserHasherService.encrypt(password);
            DataTable dt = new DataTable();

            using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
            using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:CheckUserExists"), conn))
            using (var dataAdapter = new MySqlDataAdapter(comm))
            {
                comm.CommandType = CommandType.Text;
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@encPassword", encPassword);
                dataAdapter.Fill(dt);
            }

            bool hasRows = dt.Rows.GetEnumerator().MoveNext();

            if (hasRows)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
    }
}
