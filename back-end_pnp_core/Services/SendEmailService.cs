﻿using System;
using System.Data;
using MailKit.Net.Smtp;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using MimeKit;
using back_end_pnp_core.DTOs;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IEmail
    {
        public Task<bool> sendConfirmationEmail(string email);
        public Task<bool> sendReceiptEmail(string email, CompleteTransactionDTO transaction);
        public Task<UserTokenObject> getUserTokenObject(string email);
        public Task<string> GetUserDetails(string email);
    }

    public class UserTokenObject
    {
        public string fullName { get; set; }
        public string userToken { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
    }

    public class SendEmailService : IEmail
    {
        private readonly IConfiguration _config;

        public SendEmailService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<UserTokenObject> getUserTokenObject(string email)
        {
            UserTokenObject tokenObject = new UserTokenObject();

            tokenObject.fullName = await GetUserDetails(email);
            tokenObject.userToken = UserHasherService.encrypt(email + "&" + tokenObject.fullName);
            tokenObject.name = tokenObject.fullName.Split(" ")[0];
            tokenObject.surname = tokenObject.fullName.Split(" ")[1];

            return tokenObject;
        }

        public async Task<bool> sendConfirmationEmail(string email)
        {
            try
            {
                UserTokenObject tokenObject = await getUserTokenObject(email);

                MimeMessage message = new MimeMessage();

                MailboxAddress from = new MailboxAddress("Admin", "admin@rnr.com");
                message.From.Add(from);

                MailboxAddress to = new MailboxAddress(tokenObject.fullName, email);
                message.To.Add(to);

                message.Subject = "Rick And Ray Reset Password";

                BodyBuilder bodyBuilder = new BodyBuilder();

                bodyBuilder.HtmlBody = "<h2 style='color: #B9154A;'><u>RICK AND RAY<u></h2>" +
                    "<br /><br />" +
                    "<label>Welcome " + tokenObject.name + "</label><br /><br />" +
                    "<p>Click the link to reset your password https://localhost:4200/reset/" + tokenObject.userToken +
                    "</p><br /><br /><label style='color:#B9154A;'>Please do not tamper the URL...</Label>";
                
                message.Body = bodyBuilder.ToMessageBody();
                
                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true);
                    client.Authenticate("email", "password");

                    client.Send(message);
                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
                throw new Exception();
            }
        }

        public async Task<bool> sendReceiptEmail(string email, CompleteTransactionDTO transaction)
        {
            try
            {
                bool response = false;
                UserTokenObject tokenObject = await getUserTokenObject(email);


                MimeMessage message = new MimeMessage();

                MailboxAddress from = new MailboxAddress("Admin", "admin@rnr.com");
                message.From.Add(from);

                MailboxAddress to = new MailboxAddress(tokenObject.fullName, email);
                message.To.Add(to);

                message.Subject = "Rick And Ray Reciept";

                BodyBuilder bodyBuilder = new BodyBuilder();

                bodyBuilder.HtmlBody = "<h2 style='color: #B9154A;'><u>RICK AND RAY<u></h2>" +
                    "<h3>Thank you for your purchase</h3><br />" +
                    "<label>Hi " + tokenObject.name + " " + tokenObject.surname + "</label><br />" +
                    "<p>Thank you for your purchase</p>" +
                    "<p>You have selected: " + transaction.userSelection + "</p><br />" +
                    "<p>Number Of Paid Items:" + transaction.paidItems.Length + "</p></br />";

                message.Body = bodyBuilder.ToMessageBody();

                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true);
                    client.Authenticate("email", "password");

                    client.Send(message);
                    client.Disconnect(true);
                    response = true;
                }

                return response;
            }
            catch (Exception)
            {
                return false;
                throw new Exception();
            }
        }

        public async Task<string> GetUserDetails(string email)
        {
            try
            {
                string connectionString = _config.GetValue<string>("ConnectionStrings:DB");

                DataTable dt = new DataTable();

                using (MySqlConnection conn = new MySqlConnection(connectionString))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetUserDetailsQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", email);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    string firstName = dt.Rows[0]["firstName"].ToString();
                    string lastName = dt.Rows[0]["lastName"].ToString();
                    string fullName = firstName + " " + lastName;
                    return fullName;
                }
                else
                {
                    string fullName = null;
                    return fullName;
                }
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }
    }
}
