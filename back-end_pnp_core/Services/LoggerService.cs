﻿using System;
using System.IO;
using System.Reflection;

namespace back_end_pnp_core.Services
{
    public interface ILogger
    {
        public void LogWrite(string logMessage);
        public void Log(string logMessage, TextWriter txtWriter);
    }

    public class LoggerService : ILogger
    {
        private string path = string.Empty;

        public LoggerService(string logMessage)
        {
            LogWrite(logMessage);
        }

        public void LogWrite(string logMessage)
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            try
            {
                using (StreamWriter w = File.AppendText("/Users/StaticVoid/Documents/angular_project/pnp/back-end/back-end_pnp_core/bin/Debug/netcoreapp3.1/log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public void LogWrite(string logMessage, string _value)
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            try
            {
                using (StreamWriter w = File.AppendText("/Users/StaticVoid/Documents/angular_project/pnp/back-end/back-end_pnp_core/bin/Debug/netcoreapp3.1/log.txt"))
                {
                    Log((logMessage + " " + _value), w);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("-------------------------------\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

    }
}
