﻿using AutoMapper;
using back_end_pnp_core.DTOs;
using back_end_pnp_core.Models;

namespace back_end_pnp_core.DTO_s
{
    public class ObjectMapper : Profile
    {
        public ObjectMapper()
        {
            CreateMap<Address, AddressDTO>();
            CreateMap<Cart, CartDTO>();
            CreateMap<CartItem, CartItemDTO>();
            CreateMap<CartItemUpdate, CartItemUpdateDTO>();
            CreateMap<Catalogue, CatalogueDTO>();
            CreateMap<CompleteReset, CompleteResetDTO>();
            CreateMap<CompleteResponse, CompleteResponseDTO>();
            CreateMap<Favorites, FavoritesDTO>();
            CreateMap<Header, HeaderDTO>();
            CreateMap<Ingredients, IngredientsDTO>();
            CreateMap<Lists, ListsDTO>();
            CreateMap<LoginResponse, LoginResponseDTO>();
            CreateMap<Product, ProductDTO>();
            CreateMap<ProductUpdate, ProductUpdateDTO>();
            CreateMap<Recipe, RecipeDTO>();
            CreateMap<RecipeComplete, RecipeCompleteDTO>();
            CreateMap<ResetResponse, ResetResponseDTO>();
            CreateMap<SearchRequest, SearchRequestDTO>();
            CreateMap<SearchResponse, SearchResponseDTO>();
            CreateMap<ShoppingList, ShoppingListDTO>();
            CreateMap<SignupResponse, SignupResponseDTO>();
            CreateMap<User, UserDTO>();
            CreateMap<UserUpdate, UserUpdateDTO>();
            CreateMap<CompleteTransaction, CompleteTransactionDTO>();
            CreateMap<DeliveryItem, DeliveryItemDTO>();
            CreateMap<CollectionItem, CollectionItemDTO>();
        }
    }
}
