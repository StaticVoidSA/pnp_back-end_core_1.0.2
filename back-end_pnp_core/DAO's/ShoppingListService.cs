﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IShoppingList
    {
        public Task<bool> CreateList(ShoppingListDTO list);
        public Task<List<ListsDTO>> GetUserLists(int userID);
        public Task<bool> DeleteShoppingList(int itemID, int userID);
        public Task<bool> AddToShoppingList(ShoppingListDTO list);
        public Task<List<ShoppingListDTO>> GetListItems(int userID, string listName);
        public Task<bool> DeleteFromShoppingList(int listID, int ID);
        public Task<UserDTO> GetUserDetails(int userID);
        public Task<int> GetListsCount(int userID);
    }

    public class ShoppingListService : IShoppingList
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Shopping List Service Called...");

        public ShoppingListService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<bool> CreateList(ShoppingListDTO list)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:CreateShoppingListQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listName", list.shoppingListName);
                    comm.Parameters.AddWithValue("@userID", list.userID);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Created Shopping List: " + list.shoppingListName + " For User: " + list.userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Create Shopping List: " + list.shoppingListName + " For User: " + list.userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<ListsDTO>> GetUserLists(int userID)
        {
            List<ListsDTO> response = new List<ListsDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetUserShoppingLists"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    conn.Open();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ListsDTO resp = new ListsDTO();
                        resp.userID = (int)dt.Rows[i]["userListID"];
                        resp.itemID = (int)dt.Rows[i]["ID"];
                        resp.listName = dt.Rows[i]["listName"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Returned Shopping Lists For User: " + userID);
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Shopping Lists For User: " + userID);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> DeleteShoppingList(int itemID, int userID)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:DeleteShoppingListQuery"), conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@itemID", itemID);
                    comm.Parameters.AddWithValue("@userID", userID);

                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Deleted Shopping List: " + itemID + " For User: " + userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Delete Shopping List: " + itemID + " For User: " + userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> AddToShoppingList(ShoppingListDTO list)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:AddToShoppingListQuery"), conn))
                {
                    
                    comm.CommandType = CommandType.Text;

                    comm.Parameters.AddWithValue("@title", list.title);
                    comm.Parameters.AddWithValue("@brand", list.brand);
                    comm.Parameters.AddWithValue("@barcode", list.barcode);
                    comm.Parameters.AddWithValue("@quantity", list.quantity);
                    comm.Parameters.AddWithValue("@price", list.price);
                    comm.Parameters.AddWithValue("@listName", list.shoppingListName);
                    comm.Parameters.AddWithValue("@listID", list.shoppingListID);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Added Item: " + list.itemID + " To List: " + list.shoppingListName + " For User: " + list.userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Add Item: " + list.itemID + " To List: " + list.shoppingListName + " For User: " + list.userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<ShoppingListDTO>> GetListItems(int userID, string listName)
        {
            List<ShoppingListDTO> response = new List<ShoppingListDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetListItems"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listID", userID);
                    comm.Parameters.AddWithValue("@listName", listName);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ShoppingListDTO resp = new ShoppingListDTO();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.itemID = (int)dt.Rows[i]["ID"];
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.quantity = (int)dt.Rows[i]["quantity"];
                        resp.title = dt.Rows[i]["productTitle"].ToString();
                        resp.shoppingListID = (int)dt.Rows[i]["listID"];
                        resp.shoppingListName = dt.Rows[i]["listName"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned List Items From List: " + listName + " For User: " + userID);
                    return response;
                }
                else
                {
                    logger.LogWrite("No List Items To Return From List: " + listName + " For User: " + userID);
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> DeleteFromShoppingList(int listID, int ID)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:DeleteFromShoppingListQuery"), conn))
                {
                    conn.Open();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listID", listID);
                    comm.Parameters.AddWithValue("@ID", ID);

                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Deleted Shopping List: " + listID + " For User: " + ID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Delete Shopping List: " + listID + " For User: " + ID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<UserDTO> GetUserDetails(int userID)
        {
            UserDTO response = new UserDTO();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:ShoppingListGetUserDetailsQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        response.firstName = dt.Rows[i]["firstName"].ToString();
                        response.surname = dt.Rows[i]["lastName"].ToString();
                        response.email = dt.Rows[i]["email"].ToString();
                        response.userRole = dt.Rows[i]["userRole"].ToString();
                        response.userID = (int)dt.Rows[i]["userID"];
                        response.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                    }

                    logger.LogWrite("Successfully Returned User Details For User: " + userID);
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("Unable To Return User Details For User: " + userID);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<int> GetListsCount(int userID)
        {
            int result = 0;
            DataTable dt = new DataTable();

            using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
            using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetListsCountQuery"), conn))
            using (var dataAdapter = new MySqlDataAdapter(comm))
            {
                conn.Open();
                comm.Parameters.AddWithValue("@userID", userID);
                dataAdapter.Fill(dt);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                result++;
            }

            return result;
        }
    }
}
