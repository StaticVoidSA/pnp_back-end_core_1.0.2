﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using back_end_pnp_core.DTOs;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace back_end_pnp_core.DAOs
{
    public interface IDeliveriesService
    {
        public Task<List<DeliveryItemDTO>> getDeliveries(int userID);
    }

    public class DeliveriesService : IDeliveriesService
    {
        private readonly IConfiguration _config;

        public DeliveriesService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<DeliveryItemDTO>> getDeliveries(int userID)
        {
            List<DeliveryItemDTO> response = new List<DeliveryItemDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetDeliveries"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    conn.Open();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DeliveryItemDTO item = new DeliveryItemDTO();
                        item.userID = (int)dt.Rows[i]["userID"];
                        item.deliveryID = (int)dt.Rows[i]["deliveryID"];
                        item.userName = dt.Rows[i]["userName"].ToString();
                        item.brand = dt.Rows[i]["brand"].ToString();
                        item.selectedAddress = dt.Rows[i]["selectedAddress"].ToString();
                        item.deliveryDate = Convert.ToDateTime(dt.Rows[i]["deliveryDate"]);
                        item.title = dt.Rows[i]["title"].ToString();
                        item.quantity = (int)dt.Rows[i]["quantity"];
                        item.price = Convert.ToDouble(dt.Rows[i]["price"]);

                        response.Add(item);
                    }
                    return response;
                }
                else
                {
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
