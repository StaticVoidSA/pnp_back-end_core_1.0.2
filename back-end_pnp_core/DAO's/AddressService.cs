﻿using back_end_pnp_core.DTO_s;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System;

namespace back_end_pnp_core.Services
{
    public enum AddressAdded
    {
        True,
        False,
        Exists
    }

    public interface IAddress
    {
        public Task<string> createAddress(AddressDTO address);
        public Task<List<AddressDTO>> getAddresses(int userID);
        public Task<bool> deleteAddress(int addressID, int userID);
        public Task<bool> updateAddress(AddressDTO address);
    }

    public class AddressService : IAddress
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Address Controller Called...");

        public AddressService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<string> createAddress(AddressDTO address)
        {
            int result;
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand commExists = new MySqlCommand(_config.GetValue<string>("Queries:addressExistsQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(commExists))
                {
                    commExists.CommandType = CommandType.Text;

                    commExists.Parameters.AddWithValue("@userID", address.addressID);
                    commExists.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                    commExists.Parameters.AddWithValue("@userAddress", address.userAddress);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    logger.LogWrite("Error:\nAddress With Nick Name: " + address.addressNickName + " Already Exists For User: " + address.addressID);
                    return nameof(AddressAdded.Exists);
                }
                else 
                {
                    using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                    {
                        MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:addAddressQuery"), conn);
                        comm.CommandType = CommandType.Text;

                        comm.Parameters.AddWithValue("@userID", address.addressID);
                        comm.Parameters.AddWithValue("@userAddress", address.userAddress);
                        comm.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                        comm.Parameters.AddWithValue("@isDefault", address.isDefault);

                        conn.Open();
                        result = comm.ExecuteNonQuery();
                    }

                    if (result > 0)
                    {
                        logger.LogWrite("Created Address For User: " + address.addressID);
                        return nameof(AddressAdded.True);
                    }
                    else
                    {
                        logger.LogWrite("Unable To Create Address For User: " + address.addressID);
                        return nameof(AddressAdded.False);
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<AddressDTO>> getAddresses(int userID)
        {
            List<AddressDTO> response = new List<AddressDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:getAddressesQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddressDTO resp = new AddressDTO();
                        resp.addressID = (int)dt.Rows[i]["addressID"];
                        resp.userAddress = dt.Rows[i]["userAddress"].ToString();
                        resp.ID = (int)dt.Rows[i]["ID"];
                        resp.addressNickName = dt.Rows[i]["addressNickName"].ToString();
                        resp.isDefault = dt.Rows[i]["isDefault"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Returned Addresses For User: " + userID);
                    return response;
                }
                else
                {
                    logger.LogWrite("No Addresses For User: " + userID);
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> deleteAddress(int addressID, int userID)
        {
            int response;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:deleteAddressQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@addressID", addressID);
                    comm.Parameters.AddWithValue("@userID", userID);

                    conn.Open();
                    response = comm.ExecuteNonQuery();
                }

                if (response > 0)
                {
                    logger.LogWrite("Deleted Address: " + addressID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Delete Address: " + addressID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> updateAddress(AddressDTO address)
        {
            int response = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:updateAddressQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userAddress", address.userAddress);
                    comm.Parameters.AddWithValue("@ID", address.ID);
                    comm.Parameters.AddWithValue("@userID", address.addressID);
                    comm.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                    comm.Parameters.AddWithValue("@isDefault", address.isDefault);

                    conn.Open();
                    response = comm.ExecuteNonQuery();
                }

                if (response > 0)
                {
                    logger.LogWrite("Successfully Updated Address: " + address.addressNickName + " For User: " + address.addressID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Update Address: " + address.addressNickName + " For User: " + address.addressID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
