﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface ICart
    {
        public Task<bool> AddToCart(CartDTO item);
        public Task<bool> RemoveFromCart(int cartItemID, int cartID);
        public Task<List<CartItemDTO>> getCartItems(int userID);
        public Task<int> cartCount(int userID);
        public Task<bool> updateCartItem(CartItemUpdateDTO item);
    }

    public class CartService : ICart
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Cart Service Called...");

        public CartService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<bool> AddToCart(CartDTO item)
        {
            int resultCart;
            int resultCartItems;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                {
                    MySqlCommand commCart = new MySqlCommand(_config.GetValue<string>("Queries:AddCartQuery"), conn);
                    MySqlCommand commCartItems = new MySqlCommand(_config.GetValue<string>("Queries:AddCartItemQuery"), conn);

                    commCart.CommandType = CommandType.Text;
                    commCartItems.CommandType = CommandType.Text;
                    commCart.Parameters.AddWithValue("@userID", item.userID);
                    commCartItems.Parameters.AddWithValue("@cartID", item.cartID);
                    commCartItems.Parameters.AddWithValue("@userID", item.userID);
                    commCartItems.Parameters.AddWithValue("@productTitle", item.title);
                    commCartItems.Parameters.AddWithValue("@brand", item.brand);
                    commCartItems.Parameters.AddWithValue("@barcode", item.barcode);
                    commCartItems.Parameters.AddWithValue("@quantity", item.quantity);
                    commCartItems.Parameters.AddWithValue("@price", item.price);

                    conn.Open();
                    resultCart = commCart.ExecuteNonQuery();
                    resultCartItems = commCartItems.ExecuteNonQuery();
                }

                if (resultCart > 0 && resultCartItems > 0)
                {
                    logger.LogWrite("Added " + item.barcode + " Successfully To Cart: " + item.userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Add " + item.barcode + " Successfully To Cart: " + item.userID);
                    return false;
                }
            }
            catch (Exception ex) 
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> RemoveFromCart(int cartItemID, int cartID)
        {
            int result;
            string query = "DELETE FROM cartItems WHERE cartID = '" + cartID + "' AND ID = '" + cartItemID + "'";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    comm.CommandType = CommandType.Text;

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Removed " + cartItemID + " From Cart " + cartID + " Successfully");
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Remove " + cartItemID + " From Cart " + cartID + " Successfully");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<CartItemDTO>> getCartItems(int userID)
        {
            DataTable dt = new DataTable();
            List<CartItemDTO> cartItems = new List<CartItemDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetCartItems"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    dataAdapter.Fill(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CartItemDTO result = new CartItemDTO();
                    result.userID = (int)dt.Rows[i]["cartID"];
                    result.cartItemID = (int)dt.Rows[i]["ID"];
                    result.brand = dt.Rows[i]["brand"].ToString();
                    result.title = dt.Rows[i]["productTitle"].ToString();
                    result.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    result.quantity = (int)dt.Rows[i]["quantity"];
                    result.barcode = dt.Rows[i]["barcode"].ToString();

                    cartItems.Add(result);
                }

                logger.LogWrite("Returned Cart Items For User: " + userID);
                return cartItems;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<int> cartCount(int userID)
        {
            try
            {
                if (userID != 0)
                {
                    DataTable dt = new DataTable();
                    int count = 0;

                    using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                    using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetCartItems"), conn))
                    using (var dataAdapter = new MySqlDataAdapter(comm))
                    {
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@userID", userID);
                        dataAdapter.Fill(dt);
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        count += (int)dt.Rows[i]["quantity"];
                    }

                    logger.LogWrite("Cart Count For User: " + userID + " = " + count);
                    return count;
                }
                else
                {
                    logger.LogWrite("No Cart Items For " + userID);
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> updateCartItem(CartItemUpdateDTO item)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:UpdateCartItem"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@quantity", item.quantity);
                    comm.Parameters.AddWithValue("@userID", item.userID);
                    comm.Parameters.AddWithValue("@cartItemID", item.cartItemID);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Updated Cart Item " + item.cartItemID + " For User: " + item.userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Update Cart Item " + item.cartItemID + " For User: " + item.userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> removeAllItems(int userID)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:ClearCart"), conn))
                {

                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    comm.Parameters.AddWithValue("@cartID", userID);

                    conn.Open();
                    result = comm.ExecuteNonQuery();

                    if (result > 0)
                    {
                        logger.LogWrite("All Cart Items Removed From " + userID);
                        return true;
                    }
                    else
                    {
                        logger.LogWrite("Unable To Remove All Cart Items For " + userID);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
