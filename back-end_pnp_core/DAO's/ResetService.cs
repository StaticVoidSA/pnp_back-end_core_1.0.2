﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IReset
    {
        public Task<ResetResponseDTO> resetPassword(UserDTO user);
        public Task<CompleteResponseDTO> complete(CompleteResetDTO user);
    }

    public class ResetService : IReset
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Reset Service Called...");

        public ResetService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<ResetResponseDTO> resetPassword(UserDTO user)
        {
            SendEmailService sendEmail = new SendEmailService(_config);

            try
            {
                ResetResponseDTO response = new ResetResponseDTO();
                DataTable dt = new DataTable();
                response.email = user.email;
                logger.LogWrite("User: " + user.email + " Has Requested A Password Change...");

                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:ResetPasswordQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    sendEmail.sendConfirmationEmail(response.email);
                    response.emailSent = true;
                    logger.LogWrite("Email Sent To " + user.email + " Successfully");
                    return response; 
                }
                else
                {
                    response.emailSent = false;
                    logger.LogWrite("Unable To Sent Email To " + user.email);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<CompleteResponseDTO> complete(CompleteResetDTO user)
        {
            try
            {
                CompleteResponseDTO response = new CompleteResponseDTO();
                var encPassword = UserHasherService.encrypt(user.password);
                string reset_hash = user.hash;

                SendEmailService detailsService = new SendEmailService(_config);
                string userFullName = await detailsService.GetUserDetails(user.email);
                string userResetHash = UserHasherService.encrypt(user.email + "&" + userFullName);

                if (reset_hash == userResetHash)
                {
                    logger.LogWrite("User Reset Hash Matches Request: ", userResetHash);

                    using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                    using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:CompleteResetPasswordQuery"), conn))
                    {
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@encPassword", encPassword);
                        comm.Parameters.AddWithValue("@email", user.email);

                        comm.Connection.Open();
                        int result = comm.ExecuteNonQuery();

                        if (result != 0)
                        {
                            response.email = user.email;
                            response.passwordChanged = true;
                        }
                        else
                        {
                            response.email = user.email;
                            response.passwordChanged = false;
                        }

                        return response;
                    }
                }
                else
                {
                    logger.LogWrite("User Reset Hash Does Not Match: ", userResetHash);
                    response.passwordChanged = false;
                    return response;
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }
}
