﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace back_end_pnp_core.Models
{
    public interface IAuth
    {
        public Task<List<StartupResponseDTO>> getUsers();
        public Task<SignupResponseDTO> signup(UserDTO user);
        public Task<LoginResponseDTO> login(UserDTO user, string token);
    }

    public class AuthService : IAuth
    {
        private readonly IConfiguration _config;
        private LoggerService logger = new LoggerService("Authentication Service Called...");
        private LocalStorageService storageService;

        public AuthService(IConfiguration config)
        {
            this._config = config;
            storageService = new LocalStorageService();
        }

        public async Task<List<StartupResponseDTO>> getUsers()
        {
            List<StartupResponseDTO> response = new List<StartupResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:StartupQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StartupResponseDTO resp = new StartupResponseDTO();
                    resp.userID = (int)dt.Rows[i]["userID"];
                    resp.firstName = dt.Rows[i]["firstName"].ToString();
                    resp.surname = dt.Rows[i]["lastName"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                    resp.userRole = dt.Rows[i]["userRole"].ToString();

                    response.Add(resp);
                }

                logger.LogWrite("Returned Users To Admin");
                return response.ToList();

            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<SignupResponseDTO> signup(UserDTO user)
        {
            try
            {
                SignupResponseDTO response = new SignupResponseDTO();
                DataTable dt = new DataTable();
                string encPassword = UserHasherService.encrypt(user.password);

                response.firstName = user.firstName;
                response.surname = user.surname;
                response.email = user.email;
                response.encPassword = encPassword;
                response.success = true;
                DateTime doj = DateTime.Now;

                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:SignupQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@firstName", response.firstName);
                    comm.Parameters.AddWithValue("@lastName", response.surname);
                    comm.Parameters.AddWithValue("@email", response.email);
                    comm.Parameters.AddWithValue("@encPassword", response.encPassword);
                    comm.Parameters.AddWithValue("@doj", doj);
                    dataAdapter.Fill(dt);
                }

                logger.LogWrite("New User Singup: " + user.email);
                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<LoginResponseDTO> login(UserDTO user, string token)
        {
            var encPassword = UserHasherService.encrypt(user.password);
            DataTable dt = new DataTable();
            UsersService userService = new UsersService(_config);
            
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:LoginQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    comm.Parameters.AddWithValue("@encPassword", encPassword);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    JWTService service = new JWTService(_config);
                    UserDTO currentUser = await userService.getUser(user);
    
                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = (2 * 60 * 60).ToString();
                    response.userId = (int)dt.Rows[0]["userID"];
                    response.userName = dt.Rows[0]["firstName"].ToString();
                    response.userSurname = dt.Rows[0]["lastName"].ToString();
                    response.doj = dt.Rows[0]["doj"].ToString();
                    response.userRole = dt.Rows[0]["userRole"].ToString();
                    response.token = token;
                    response.success = true;

                    storageService.write("Bearer " + response.token);
                    logger.LogWrite("User Successfully Logged In: " + user.email + "\nToken Generated: " + response.token);
                    return response;
                }
                else
                {
                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = null;
                    response.userName = user.email;
                    response.userRole = string.Empty;
                    response.token = "";
                    response.success = false;

                    logger.LogWrite("Unable To Log In User: " + user.email);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
