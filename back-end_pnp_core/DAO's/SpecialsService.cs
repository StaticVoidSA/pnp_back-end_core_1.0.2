﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface ISpecials
    {
        public Task<List<SpecialsDTO>> getSpecials(string category);
        public string getDiscount(double price);
    }

    public class SpecialsService : ISpecials
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Specials Service Called...");

        public SpecialsService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<SpecialsDTO>> getSpecials(string category) 
        {
            DataTable dt = new DataTable();
            List<SpecialsDTO> response = new List<SpecialsDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetSpecials"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@category", category);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        SpecialsDTO resp = new SpecialsDTO();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.discount = getDiscount(Convert.ToDouble(dt.Rows[i]["price"]));
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.oldPrice = Convert.ToDouble(resp.price + Convert.ToDouble(resp.discount));
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned Specials");
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Specials To Return");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public string getDiscount(double price) 
        {
            return (Math.Round((price * 0.25), 2)).ToString("N2");
        }
    }
}
