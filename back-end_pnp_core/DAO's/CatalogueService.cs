﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface ICatalogue
    {
        public Task<List<CatalogueDTO>> getCatalogues(string category);
    }

    public class CatalogueService : ICatalogue
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Catalogue Service Called...");

        public CatalogueService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<CatalogueDTO>> getCatalogues(string category)
        {
            string query = "SELECT brand, category FROM Catalogue WHERE uri LIKE '%" + category + "%'";
            DataTable dt = new DataTable();
            List<CatalogueDTO> response = new List<CatalogueDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CatalogueDTO resp = new CatalogueDTO();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Returned Catalogues Successfully");
                    return response;
                }
                else
                {
                    logger.LogWrite("Unable To Return Catalogues Successfully");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
