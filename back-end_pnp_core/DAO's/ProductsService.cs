﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IProducts
    {
        public Task<ProductDTO> getProduct(ProductDTO product);
        public Task<List<SearchResponseDTO>> getProducts();
        public Task<bool> deleteProduct(ProductDTO product);
        public Task<bool> editProduct(ProductUpdateDTO product);
    }

    public class ProductsService : IProducts
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Products Service Called...");

        public ProductsService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<ProductDTO> getProduct(ProductDTO product)
        {
            DataTable dt = new DataTable();
            ProductDTO response = new ProductDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetProductsQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@barcode", product.barcode);
                    dataAdapter.Fill(dt);
                }

                bool hasProduct = dt.Rows.GetEnumerator().MoveNext();

                if (hasProduct)
                {
                    response.productID = (int)dt.Rows[0]["productID"];
                    response.title = dt.Rows[0]["title"].ToString();
                    response.brand = dt.Rows[0]["brand"].ToString();
                    response.category = dt.Rows[0]["category"].ToString();
                    response.uri = dt.Rows[0]["uri"].ToString();
                    response.price = Convert.ToDouble(dt.Rows[0]["price"]);
                    response.description = dt.Rows[0]["descr"].ToString();
                    response.features = dt.Rows[0]["features"].ToString();
                    response.usage = dt.Rows[0]["_usage"].ToString();
                    response.quantity = dt.Rows[0]["quantity"].ToString();
                    response.barcode = dt.Rows[0]["barcode"].ToString();

                    logger.LogWrite("Get Product: " + product.barcode + " Successfully");
                    return response;
                }
                else
                {
                    logger.LogWrite("Get Product: " + product.barcode + " No Product");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<SearchResponseDTO>> getProducts()
        {
            List<SearchResponseDTO> response = new List<SearchResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:ProductsQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    resp.productID = (int)dt.Rows[i]["productID"];
                    resp.title = dt.Rows[i]["title"].ToString();
                    resp.category = dt.Rows[i]["category"].ToString();
                    resp.brand = dt.Rows[i]["brand"].ToString();
                    resp.uri = dt.Rows[i]["uri"].ToString();
                    resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    resp.description = dt.Rows[i]["descr"].ToString();
                    resp.features = dt.Rows[i]["features"].ToString();
                    resp.usage = dt.Rows[i]["_usage"].ToString();
                    resp.quantity = dt.Rows[i]["quantity"].ToString();
                    resp.barcode = dt.Rows[i]["barcode"].ToString();

                    response.Add(resp);
                }

                logger.LogWrite("Get Products Successful...");
                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> deleteProduct(ProductDTO product)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:DeleteProductQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@barcode", product.barcode);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result == -1)
                {
                    logger.LogWrite("Successfully Deleted Product: " + product.barcode);
                    return false;
                }
                else
                {
                    logger.LogWrite("Unable To Delete Product: " + product.barcode);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> editProduct(ProductUpdateDTO product)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:UpdateProductQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@title", product.title);
                    comm.Parameters.AddWithValue("@category", product.category);
                    comm.Parameters.AddWithValue("@brand", product.brand);
                    comm.Parameters.AddWithValue("@uri", product.uri);
                    comm.Parameters.AddWithValue("@price", Convert.ToDecimal(product.price));
                    comm.Parameters.AddWithValue("@description", product.description);
                    comm.Parameters.AddWithValue("@features", product.features);
                    comm.Parameters.AddWithValue("@usage", product.usage);
                    comm.Parameters.AddWithValue("@quantity", product.quantity);
                    comm.Parameters.AddWithValue("@barcode", product.barcode);
                    comm.Parameters.AddWithValue("@_barcode", product._barcode);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Edited Product: " + product.barcode);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Edit Product: " + product.barcode);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
