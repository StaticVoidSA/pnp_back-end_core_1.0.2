﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using back_end_pnp_core.DTOs;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace back_end_pnp_core.DAOs
{
    public interface ICollectionService
    {
        public Task<List<CollectionItemDTO>> getCollections(int userID);
    }

    public class CollectionService : ICollectionService
    {
        private readonly IConfiguration _config;

        public CollectionService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<CollectionItemDTO>> getCollections(int userID)
        {
            List<CollectionItemDTO> response = new List<CollectionItemDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetCollections"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    conn.Open();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CollectionItemDTO item = new CollectionItemDTO();

                        item.userID = (int)dt.Rows[i]["userID"];
                        item.userName = dt.Rows[i]["userName"].ToString();
                        item.brand = dt.Rows[i]["brand"].ToString();
                        item.title = dt.Rows[i]["title"].ToString();
                        item.quantity = (int)dt.Rows[i]["quantity"];
                        item.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        item.selectedShop = dt.Rows[i]["selectedShop"].ToString();
                        item.selectedCollectionDate = Convert.ToDateTime(dt.Rows[i]["selectedCollectionDate"]);
                        item.transactionDate = Convert.ToDateTime(dt.Rows[i]["transactionDate"]);

                        response.Add(item);
                    }

                    return response;
                }
                else
                {
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
