﻿using System;
using System.Threading.Tasks;
using back_end_pnp_core.DTOs;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace back_end_pnp_core.DAOs
{
    public interface ICompleteTransaction
    {
        public Task<bool> CompleteCollection(CompleteTransactionDTO input);
        public Task<bool> CompleteDelivery(CompleteTransactionDTO input);
    }

    public class CompleteTransactionService : ICompleteTransaction
    {
        private readonly IConfiguration _config;
        PaidItemsService service;
        LoggerService logger = new LoggerService("Complete Transaction Service Called...");

        public CompleteTransactionService(IConfiguration config)
        {
            this._config = config;
            service = new PaidItemsService(_config);
        }

        public async Task<bool> CompleteCollection(CompleteTransactionDTO input)
        {
            bool response = false;
            int output;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand commCollection = new MySqlCommand(_config.GetValue<string>("Queries:AddCollection"), conn))
                using (MySqlCommand commCollectionItems = new MySqlCommand(_config.GetValue<string>("Queries:AddCollectionItems"), conn))
                {
                    conn.Open();
                    commCollection.CommandType = System.Data.CommandType.Text;
                    commCollectionItems.CommandType = System.Data.CommandType.Text;

                    commCollection.Parameters.AddWithValue("@collectionID", input.userID);
                    commCollection.Parameters.AddWithValue("@userID", input.userID);
                    commCollection.Parameters.AddWithValue("@userName", input.userName);
                    commCollection.Parameters.AddWithValue("@selectedShop", input.selectedShop);
                    commCollection.Parameters.AddWithValue("@selectedCollectionDate", input.selectedCollectionDate);
                    commCollection.Parameters.AddWithValue("@transactionDate", input.transactionDate);

                    output = commCollection.ExecuteNonQuery();

                    if (output > 0)
                    {
                        for (int i = 0; i < input.paidItems.Length; i++)
                        {
                            commCollectionItems.Parameters.Clear();

                            commCollectionItems.Parameters.AddWithValue("@collectionID", input.userID);
                            commCollectionItems.Parameters.AddWithValue("@title", input.paidItems[i].title);
                            commCollectionItems.Parameters.AddWithValue("@brand", input.paidItems[i].brand);
                            commCollectionItems.Parameters.AddWithValue("@quantity", input.paidItems[i].quantity);
                            commCollectionItems.Parameters.AddWithValue("@price", input.paidItems[i].price);
                            commCollectionItems.Parameters.AddWithValue("@productID", input.paidItems[i].productID);

                            commCollectionItems.ExecuteNonQuery();
                        }

                        logger.LogWrite("User " + input.userID + " Selected Collection For " + input.paidItems.Length + " items");
                        response = await service.ClearPaidItems(input.userID); 
                    }
                    else
                    {
                        response = false;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> CompleteDelivery(CompleteTransactionDTO input)
        {
            bool response = false;
            int output;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand commDelivery = new MySqlCommand(_config.GetValue<string>("Queries:AddDelivery"), conn))
                using (MySqlCommand commDeliveryItems = new MySqlCommand(_config.GetValue<string>("Queries:AddDeliveryItems"), conn))
                {
                    conn.Open();
                    commDelivery.CommandType = System.Data.CommandType.Text;
                    commDeliveryItems.CommandType = System.Data.CommandType.Text;

                    commDelivery.Parameters.AddWithValue("@deliveryID", input.userID);
                    commDelivery.Parameters.AddWithValue("@userID", input.userID);
                    commDelivery.Parameters.AddWithValue("@userName", input.userName);
                    commDelivery.Parameters.AddWithValue("@selectedAddress", input.selectedAddress);
                    commDelivery.Parameters.AddWithValue("@deliveryDate", input.deliveryDate);

                    output = commDelivery.ExecuteNonQuery();

                    if (output > 0)
                    {
                        for (int i = 0; i < input.paidItems.Length; i++)
                        {
                            commDeliveryItems.Parameters.Clear();

                            commDeliveryItems.Parameters.AddWithValue("@deliveryID", input.userID);
                            commDeliveryItems.Parameters.AddWithValue("@title", input.paidItems[i].title);
                            commDeliveryItems.Parameters.AddWithValue("@brand", input.paidItems[i].brand);
                            commDeliveryItems.Parameters.AddWithValue("@quantity", input.paidItems[i].quantity);
                            commDeliveryItems.Parameters.AddWithValue("@price", input.paidItems[i].price);
                            commDeliveryItems.Parameters.AddWithValue("@productID", input.paidItems[i].productID);

                            commDeliveryItems.ExecuteNonQuery();
                        }

                        logger.LogWrite("User " + input.userID + " Selected Delivery For " + input.paidItems.Length + " items");
                        response = await service.ClearPaidItems(input.userID);
                    }
                    else
                    {
                        response = false;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
