﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IFavorites
    {
        public Task<List<FavoritesDTO>> getFavorites(int userID);
        public Task<bool> addToFavorites(FavoritesDTO item);
        public Task<bool> removeFromFavorites(int ID, int favID);
        public Task<bool> addToCart(CartDTO item);
    }

    public class FavoritesService : IFavorites
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Favorites Service Called...");

        public FavoritesService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<FavoritesDTO>> getFavorites(int userID)
        {
            List<FavoritesDTO> response = new List<FavoritesDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetFavoritesQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FavoritesDTO resp = new FavoritesDTO();
                        resp.userID = (int)dt.Rows[i]["favID"];
                        resp.favID = (int)dt.Rows[i]["favID"];
                        resp.ID = (int)dt.Rows[i]["ID"];
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.title = dt.Rows[i]["productTitle"].ToString();
                        resp.description = dt.Rows[i]["description"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);

                        response.Add(resp);
                    }

                    logger.LogWrite("Returned Favorites From User: " + userID);
                    return response;
                }
                else
                {
                    logger.LogWrite("No Favorites To Return For User: " + userID);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> addToFavorites(FavoritesDTO item)
        {
            bool response = false;
            int resultFav;
            int resultFavItems;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand commA = new MySqlCommand(_config.GetValue<string>("Queries:AddToFavorites1Query"), conn))
                using (MySqlCommand commB = new MySqlCommand(_config.GetValue<string>("Queries:AddToFavorites2Query"), conn))
                {
                    commA.CommandType = CommandType.Text;
                    commB.CommandType = CommandType.Text;

                    commA.Parameters.AddWithValue("@userID", item.userID);
                    commB.Parameters.AddWithValue("@favID", item.userID);
                    commB.Parameters.AddWithValue("@uri", item.uri);
                    commB.Parameters.AddWithValue("@description", item.description);
                    commB.Parameters.AddWithValue("@productTitle", item.title);
                    commB.Parameters.AddWithValue("@brand", item.brand);
                    commB.Parameters.AddWithValue("@barcode", item.barcode);
                    commB.Parameters.AddWithValue("@quantity", item.quantity);
                    commB.Parameters.AddWithValue("@price", item.price);
                    commB.Parameters.AddWithValue("@productID", item.productID);

                    conn.Open();
                    resultFav = commA.ExecuteNonQuery();
                    resultFavItems = commB.ExecuteNonQuery();

                    commA.Parameters.Clear();
                    commB.Parameters.Clear();

                    conn.Close();
                }

                if (resultFav > 0 && resultFavItems > 0)
                {
                    response = true;
                    logger.LogWrite("Added " + item.barcode + " Successfully To Favorites For User: " + item.userID);
                    return response;
                }
                else
                {
                    logger.LogWrite("Unable To Add " + item.barcode + " Successfully To Favorites For User: " + item.userID);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> removeFromFavorites(int ID, int favID)
        {
            bool result = false;
            int resultFavorite;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:RemoveFromFavoritesQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@ID", ID);
                    comm.Parameters.AddWithValue("@favID", favID);

                    conn.Open();
                    resultFavorite = comm.ExecuteNonQuery();
                    conn.Close();
                }

                if (resultFavorite > 0)
                {
                    result = true;
                    logger.LogWrite("Removed " + favID + " Successfully From Favorites For User: " + ID);
                    return result;
                }
                else
                {
                    logger.LogWrite("Unable To Remove " + favID + " Successfully From Favorites For User: " + ID);
                    return result;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> addToCart(CartDTO item)
        {
            int resultCart;
            int resultCartItems;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                {
                    MySqlCommand commCart = new MySqlCommand(_config.GetValue<string>("Queries:AddCartQuery"), conn);
                    MySqlCommand commCartItems = new MySqlCommand(_config.GetValue<string>("Queries:AddCartItemQuery"), conn);

                    commCart.CommandType = CommandType.Text;
                    commCartItems.CommandType = CommandType.Text;
                    commCart.Parameters.AddWithValue("@userID", item.userID);
                    commCartItems.Parameters.AddWithValue("@cartID", item.userID);
                    commCartItems.Parameters.AddWithValue("@userID", item.userID);
                    commCartItems.Parameters.AddWithValue("@productTitle", item.title);
                    commCartItems.Parameters.AddWithValue("@brand", item.brand);
                    commCartItems.Parameters.AddWithValue("@barcode", item.barcode);
                    commCartItems.Parameters.AddWithValue("@quantity", item.quantity);
                    commCartItems.Parameters.AddWithValue("@price", item.price);

                    conn.Open();
                    resultCart = commCart.ExecuteNonQuery();
                    resultCartItems = commCartItems.ExecuteNonQuery();
                }

                if (resultCart > 0 && resultCartItems > 0)
                {
                    logger.LogWrite("Removed " + item.barcode + " From Cart From User: " + item.userID);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable To Remove " + item.barcode + " From Cart From User: " + item.userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
