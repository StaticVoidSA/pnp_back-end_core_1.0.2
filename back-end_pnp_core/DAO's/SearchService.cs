﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface ISearch
    {
        public Task<IEnumerable<SearchResponseDTO>> catalogue();
        public Task<List<SearchResponseDTO>> searchAll(SearchRequestDTO request);
    }

    public class SearchService : ISearch
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Search Service Called...");

        public SearchService(IConfiguration config)
        {
            this._config = config;
        }


        public async Task<IEnumerable<SearchResponseDTO>> catalogue()
        {
            string query = _config.GetValue<string>("Queries:CatalogueQuery");
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                    resp.title = dt.Rows[i]["title"].ToString();
                    resp.category = dt.Rows[i]["category"].ToString();
                    resp.brand = dt.Rows[i]["brand"].ToString();
                    resp.uri = dt.Rows[i]["uri"].ToString();
                    resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    resp.description = dt.Rows[i]["descr"].ToString();
                    resp.features = dt.Rows[i]["features"].ToString();
                    resp.usage = dt.Rows[i]["_usage"].ToString();
                    resp.quantity = dt.Rows[i]["quantity"].ToString();
                    resp.barcode = dt.Rows[i]["barcode"].ToString();

                    responseList.Add(resp);
                }

                logger.LogWrite("Catalogue Search Complete...");
                return responseList.ToList();
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }


        public async Task<List<SearchResponseDTO>> searchAll(SearchRequestDTO request)
        {
            try
            {
                DataTable dt = new DataTable();
                List<SearchResponseDTO> response = new List<SearchResponseDTO>();

                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:SearchAllQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@item", request.item);
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Search For " + request.item + " Completed Successfully");
                    return response;
                }
                else
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    logger.LogWrite("Search For " + request.item + " Unable To Complete Successfully");
                    response.Add(resp);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
