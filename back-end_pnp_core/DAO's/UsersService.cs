﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IUsers
    {
        public Task<List<UserDTO>> getUsers();
        public Task<UserDTO> getUser(UserDTO user);
        public Task<UserDTO> getUserByID(int ID);
        public Task<bool> deleteUser(UserDTO user);
        public Task<bool> updateUser(UserUpdateDTO user);
    }

    public class UsersService : IUsers
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Users Service Called...");

        public UsersService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<UserDTO>> getUsers()
        {
            List<UserDTO> response = new List<UserDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetUsers"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserDTO resp = new UserDTO();
                        resp.firstName = dt.Rows[i]["firstName"].ToString();
                        resp.surname = dt.Rows[i]["lastName"].ToString();
                        resp.email = dt.Rows[i]["email"].ToString();
                        resp.password = dt.Rows[i]["encPassword"].ToString();
                        resp.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                        resp.userRole = dt.Rows[i]["userRole"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned Users");
                    return response.ToList();
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Users To Return");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<UserDTO> getUser(UserDTO user)
        {
            DataTable dt = new DataTable();
            UserDTO resp = new UserDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetUserQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    dataAdapter.Fill(dt);
                }

                bool hasUser = dt.Rows.GetEnumerator().MoveNext();

                if (hasUser)
                {
                    resp.firstName = dt.Rows[0]["firstName"].ToString();
                    resp.surname = dt.Rows[0]["lastName"].ToString();
                    resp.email = dt.Rows[0]["email"].ToString();
                    resp.password = dt.Rows[0]["encPassword"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[0]["doj"]);
                    resp.userRole = dt.Rows[0]["userRole"].ToString();
                    resp.userID = (int)dt.Rows[0]["userID"];

                    logger.LogWrite("Successfully Returned User Details For User: " + user.userID);
                    return resp;
                }
                else
                {
                    resp.firstName = null;
                    resp.surname = null;
                    resp.email = null;
                    resp.password = null;
                    resp.doj = DateTime.Now;
                    resp.userRole = null;

                    logger.LogWrite("No Such User Exists: " + user.userID);
                    return resp;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<UserDTO> getUserByID(int ID)
        {
            DataTable dt = new DataTable();
            UserDTO resp = new UserDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetUserByIDQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", ID);
                    dataAdapter.Fill(dt);
                }

                bool hasUser = dt.Rows.GetEnumerator().MoveNext();

                if (hasUser)
                {
                    resp.firstName = dt.Rows[0]["firstName"].ToString();
                    resp.surname = dt.Rows[0]["lastName"].ToString();
                    resp.email = dt.Rows[0]["email"].ToString();
                    resp.password = dt.Rows[0]["encPassword"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[0]["doj"]);
                    resp.userRole = dt.Rows[0]["userRole"].ToString();
                    resp.userID = (int)dt.Rows[0]["userID"];
                    
                    logger.LogWrite("Successfully Returned User Details For User: " + ID);
                    return resp;
                }
                else
                {
                    resp.firstName = null;
                    resp.surname = null;
                    resp.email = null;
                    resp.password = null;
                    resp.doj = DateTime.Now;
                    resp.userRole = null;
                    resp.token = "Bearer null";

                    logger.LogWrite("No Such User Exists: " + ID);
                    return resp;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> deleteUser(UserDTO user)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:DeleteUserQuery"), conn))
                {

                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", user.userID);
                    comm.Parameters.AddWithValue("@email", user.email);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result == -1)
                {
                    logger.LogWrite("Successfully Deleted User: " + user.userID);
                    return false;
                }
                else
                {
                    logger.LogWrite("Unable To Delete User: " + user.userID);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<bool> updateUser(UserUpdateDTO user)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:UpdateUserQuery"), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@firstName", user.firstName);
                    comm.Parameters.AddWithValue("@lastName", user.surname);
                    comm.Parameters.AddWithValue("@email", user.email);
                    comm.Parameters.AddWithValue("@userRole", user.userRole);
                    comm.Parameters.AddWithValue("@_email", user._email);

                    conn.Open();
                    result = comm.ExecuteNonQuery();
                }

                if (result > 0)
                {
                    logger.LogWrite("Successfully Updated User: " + user._email);
                    return true;
                }
                else
                {
                    logger.LogWrite("Unable Update User: " + user._email);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
