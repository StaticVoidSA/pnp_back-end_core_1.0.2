﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IRecipes
    {
        public Task<List<RecipeDTO>> startupRecipes();
        public Task<List<RecipeCompleteDTO>> getRecipe(int recipeID);
        public Task<List<RecipeCompleteDTO>> getAllRecipes();
    }

    public class RecipesService : IRecipes
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Recipes Service Called...");

        public RecipesService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<RecipeDTO>> startupRecipes()
        {
            List<RecipeDTO> response = new List<RecipeDTO>();
            string query = "SELECT * FROM Recipes";
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeDTO resp = new RecipeDTO();
                        resp.recipeID = (int)dt.Rows[i]["recipeID"];
                        resp.title = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned All Recipes...");
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Recipes To Return...");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<RecipeCompleteDTO>> getRecipe(int recipeID)
        {
            DataTable dt = new DataTable();
            List<RecipeCompleteDTO> response = new List<RecipeCompleteDTO>();


            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetRecipeQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@recipeID", recipeID);

                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeCompleteDTO resp = new RecipeCompleteDTO();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();
                        resp.recipeTitle = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recID = (int)dt.Rows[i]["recID"];
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.ingredientTitle = dt.Rows[i]["ingredientTitle"].ToString();
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.ingredientImage = dt.Rows[i]["ingredientImage"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned Recipe: " + recipeID);
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Recipe Exists: " + recipeID);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<RecipeCompleteDTO>> getAllRecipes()
        {
            List<RecipeCompleteDTO> response = new List<RecipeCompleteDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetRecipesQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeCompleteDTO resp = new RecipeCompleteDTO();
                        resp.recipeTitle = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();
                        resp.recID = (int)dt.Rows[i]["recID"];
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.ingredientTitle = dt.Rows[i]["ingredientTitle"].ToString();
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.ingredientImage = dt.Rows[i]["ingredientImage"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Successfully Returned All Recipes");
                    return response;
                }
                else
                {
                    response = null;
                    logger.LogWrite("No Recipes To Return");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
