﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace back_end_pnp_core.DAOs
{

    public interface IPaidFor
    {
        public Task<bool> AddToPaidFor(CartDTO item);
        public Task<List<CartDTO>> GetPaidFor(int userID);
    }

    public class PaidItemsService : IPaidFor
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Paid For Service Called...");


        public PaidItemsService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<bool> AddToPaidFor(CartDTO item)
        { 
            using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
            using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:AddPaidFor"), conn))
            {
                int result = 0;
                comm.CommandType = CommandType.Text;

                conn.Open();

                comm.Parameters.AddWithValue("@userID", item.userID);
                comm.Parameters.AddWithValue("@title", item.title);
                comm.Parameters.AddWithValue("@brand", item.brand);
                comm.Parameters.AddWithValue("@barcode", item.barcode);
                comm.Parameters.AddWithValue("@quantity", item.quantity);
                comm.Parameters.AddWithValue("@price", item.price);
                result = comm.ExecuteNonQuery();
                comm.Parameters.Clear();

                if (result > 0)
                {
                    logger.LogWrite("Added Items to Paid Items for user: " + item.userID);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<List<CartDTO>> GetPaidFor(int userID)
        {
            DataTable dt = new DataTable();
            List<CartDTO> results = new List<CartDTO>();

            using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
            using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:GetPaidFor"), conn))
            using (var dataAdapter = new MySqlDataAdapter(comm))
            {
                comm.CommandType = CommandType.Text;
                comm.Parameters.AddWithValue("@userID", userID);

                conn.Open();
                dataAdapter.Fill(dt);
                comm.Parameters.Clear();
            }

            bool hasRows = dt.Rows.GetEnumerator().MoveNext();

            if (hasRows)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CartDTO result = new CartDTO();

                    result.userID = (int)dt.Rows[i]["pid"];
                    result.brand = dt.Rows[i]["brand"].ToString();
                    result.title = dt.Rows[i]["productTitle"].ToString();
                    result.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    result.quantity = Convert.ToInt32(dt.Rows[i]["quantity"]);
                    result.barcode = dt.Rows[i]["barcode"].ToString();

                    results.Add(result);
                }

                logger.LogWrite("Get Paid Items for user: " + userID);
                return results;
            }
            else
            {
                results = null;
                return results;
            }
        }

        public async Task<bool> ClearPaidItems(int userID)
        {
            bool result = false;
            int response = 0;

            using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
            using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:ClearPaidFor"), conn))
            {
                comm.CommandType = CommandType.Text;
                comm.Parameters.AddWithValue("@userID", userID);

                conn.Open();
                response = comm.ExecuteNonQuery();
            }

            if (response > 0)
            {
                logger.LogWrite("Paid Items successfully cleared for user: " + userID);
                result = true;
            }

            return result;
        }
    }
}
