﻿using back_end_pnp_core.DTO_s;
using back_end_pnp_core.Services;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_pnp_core.Models
{
    public interface IShop
    {
        public Task<List<SearchResponseDTO>> onStartup();
        public Task<List<SearchResponseDTO>> filterByBrand(SearchRequestDTO request);
        public Task<List<SearchResponseDTO>> filterByPrice(SearchRequestDTO request);
        public Task<List<SearchResponseDTO>> filterByQuantity(SearchRequestDTO request);
    }

    public class ShopService : IShop
    {
        private readonly IConfiguration _config;
        LoggerService logger = new LoggerService("Shop Serivice Called...");

        public ShopService(IConfiguration config)
        {
            this._config = config;
        }

        public async Task<List<SearchResponseDTO>> onStartup()
        {
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:CatalogueQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i <= 16; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }
                }

                logger.LogWrite("Start Up Completed Successfully...");
                return responseList.ToList();
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<SearchResponseDTO>> filterByBrand(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:FilterByBrandQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.Parameters.AddWithValue("@brand", request.brand);
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }

                    logger.LogWrite("Filter By Brand: " + request.brand + " On Category: " + request.category + " Completed Successfully...");
                    return responseList;
                }
                else
                {
                    logger.LogWrite("Unable To Filter By Brand: " + request.brand + " On Category: " + request.category + " Successfully...");
                    return responseList;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<SearchResponseDTO>> filterByPrice(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:FilterByPriceQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.Parameters.AddWithValue("@price", Convert.ToDouble(request.price));
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }

                    logger.LogWrite("Filter By Price: " + request.price + " On Category: " + request.category + " Completed Successfully...");
                    return responseList;
                }
                else
                {
                    logger.LogWrite("Unable To Filter By Price: " + request.price + " On Category: " + request.category + " Successfully...");
                    return responseList;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }

        public async Task<List<SearchResponseDTO>> filterByQuantity(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> response = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_config.GetValue<string>("ConnectionStrings:DB")))
                using (MySqlCommand comm = new MySqlCommand(_config.GetValue<string>("Queries:FilterByQuantityQuery"), conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    comm.Parameters.AddWithValue("@quantity", request.quantity);
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    dataAdapter.Fill(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        response.Add(resp);
                    }

                    logger.LogWrite("Filter By Quantity: " + request.quantity + " On Category: " + request.category + " Completed Successfully...");
                    return response;
                }
                else
                {
                    logger.LogWrite("Unable To Filter By Quantity: " + request.quantity + " On Category: " + request.category + " Successfully...");
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception();
            }
        }
    }
}
